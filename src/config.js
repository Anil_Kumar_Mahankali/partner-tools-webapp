import {IdentityServiceSdkConfig} from 'identity-service-sdk';
import {PartnerRepServiceSdkConfig} from 'partner-rep-service-sdk';
import {AccountPermissionsServiceSdkConfig} from 'account-permissions-service-sdk';
import {SessionManagerConfig} from 'session-manager';

export default class Config {

    _identityServiceSdkConfig:IdentityServiceSdkConfig;

    _partnerRepServiceSdkConfig:PartnerRepServiceSdkConfig;

    _accountPermissionsServiceSdkConfig:AccountPermissionsServiceSdkConfig;

    _sessionManagerConfig:SessionManagerConfig;

    /**
     * @param {IdentityServiceSdkConfig} identityServiceSdkConfig
     * @param {PartnerRepServiceSdkConfig} partnerRepServiceSdkConfig
     * @param {AccountPermissionsServiceSdkConfig} accountPermissionsServiceSdkConfig
     * @param {SessionManagerConfig} sessionManagerConfig
     */
    constructor(identityServiceSdkConfig:IdentityServiceSdkConfig,
                partnerRepServiceSdkConfig:PartnerRepServiceSdkConfig,
                accountPermissionsServiceSdkConfig:AccountPermissionsServiceSdkConfig,
                sessionManagerConfig:SessionManagerConfig) {

        if (!identityServiceSdkConfig) {
            throw new TypeError('identityServiceSdkConfig required');
        }
        this._identityServiceSdkConfig = identityServiceSdkConfig;

        if (!partnerRepServiceSdkConfig) {
            throw new TypeError('partnerRepServiceSdkConfig required');
        }
        this._partnerRepServiceSdkConfig = partnerRepServiceSdkConfig;

        if(!accountPermissionsServiceSdkConfig) {
            throw new TypeError('accountPermissionsServiceSdkConfig required');
        }
        this._accountPermissionsServiceSdkConfig = accountPermissionsServiceSdkConfig;

        if (!sessionManagerConfig) {
            throw new TypeError('sessionManagerConfig');
        }
        this._sessionManagerConfig = sessionManagerConfig;

    }

    get identityServiceSdkConfig() {
        return this._identityServiceSdkConfig;
    }

    get partnerRepServiceSdkConfig() {
        return this._partnerRepServiceSdkConfig;
    }

    get accountPermissionsServiceSdkConfig() {
        return this._accountPermissionsServiceSdkConfig;
    }

    get sessionManagerConfig() {
        return this._sessionManagerConfig;
    }
}